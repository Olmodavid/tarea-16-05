#include<iostream>
#include<time.h>
#include <stdlib.h>

using namespace std;

/*
 =====================
  Variables Globales
 =====================
 */
int opcion;
int tempInd;
bool salir = false;
char tempName [45];
const int tamanio = 50;
int cantidadEmpleados = 0;

struct Empleado {
    int id = 0;
    char nombre[45];
    char sexo[10];
    float salario;
};

struct Empleado empleados[tamanio];

/*
=====================
 Funciones
=====================
*/
int generarID();
void strToLower(char *cadena);
void eliminarEmpleadoPorId(int id);
void showEmpleado(Empleado empleado); // DRY
struct Empleado * buscarEmpleadoPorNombre(char *nombre);
void agregarEditarEmpleado(struct Empleado *empleado, bool editar); // DRY

/*
=====================
 Programa principal
=====================
*/
int main(int argc, char* argv[]){
    while( !salir ){
        //Inicia Menu
        cout << "\t\t\tCRM TunaTech" << endl;
        for (int i = 0; i < 40; i++) {
            cout << "=";
        }
         
        cout << endl << endl << "\tMenu" << endl;
        cout << endl << "\t 1.- Agregar empleado" << endl;
        cout << "\t 2.- Editar empleado" << endl;
        cout << "\t 3.- Eliminar empleado" << endl;
        cout << "\t 4.- Mostrar todos los empleados" << endl;
        cout << "\t 5.- Mostrar al empleado con el nombre" << endl;
        cout << "\t 6.- Mostrar al empleado con el id" << endl;
        cout << "\t 7.- Salir" << endl << endl;
        cout << "\t Ingrese una opcion:  " << endl << endl;
        
        cin >> opcion;
        
        switch (opcion) {
            case 1:
                agregarEditarEmpleado(&empleados[cantidadEmpleados++], false);
                fflush(stdin);
                getchar();
                break;
            case 2: {
                    fflush(stdin);
                    cout << "Ingresa nombre de empleado a editar: " << endl;
                    cin.getline(tempName, sizeof(tempName));
                
                    Empleado *emp = buscarEmpleadoPorNombre(tempName);
                    if( emp != NULL ){
                        agregarEditarEmpleado(emp, true);
                    }else {
                        cout << "No hay empleados con ese nombre." << endl;
                    }
                }
                fflush(stdin);
                getchar();
                break;
            case 3: {
                    fflush(stdin);
                    int id;
                    cout << "Ingresa el id del empleado a eliminar: " << endl;
                    cin >> id;
                    eliminarEmpleadoPorId(id);
                }
                break;
            case 4: {
                    int i = 0;
                    for(i = 0; i < cantidadEmpleados; i++ ){
                        cout << " ============================== " << endl;
                        showEmpleado(empleados[i]);
                        cout << endl;
                    }
                    
                    if( i == 0 ){
                        cout << "No hay empleados aun." << endl;
                    }
                }
                fflush(stdin);
                getchar();
                break;
            case 5: {
                    fflush(stdin);
                    cout << "Ingresa nombre de empleado a editar: " << endl;
                    cin.getline(tempName, sizeof(tempName));
                    
                    Empleado *emp = buscarEmpleadoPorNombre(tempName);
                    showEmpleado(*emp);
                }
                fflush(stdin);
                getchar();
                break;
            case 6:
                // TODO: editar empleado
                break;
            case 7:
                salir = opcion == 7;
                break;
                
            default:
                cout << "Opcion invalida. Intente de nuevo con otro numero.";
                salir = true;
                break;
        }
    }
    
    return 0;
}

int generarID(){
    // Generamos semilla
    srand((unsigned int)time(NULL));
    return rand() % 258800 + 258790;
}

void strToLower(char *cadena){
    for(int i = 0; i < strlen(cadena); i++){
        cadena[i] = tolower(cadena[i]);
    }
}

void eliminarEmpleadoPorId(int id){
    int idx =  0;
    for(idx = 0; idx < cantidadEmpleados; idx++){
        if( empleados[idx].id == id ){
            break;
        }
    }
    
    for(int i = cantidadEmpleados - 1; i > idx; i--){
        empleados[i - 1] = empleados[i];
    }
    
    cantidadEmpleados--;
}

void showEmpleado(Empleado empleado){
    cout.precision(2);
    
    cout << "Empleado: " << endl;
    cout << " ============================== " << endl;
    cout << "Id: " << empleado.id << endl;
    cout << "Nombre: " << empleado.nombre << endl;
    cout << "Sexo: " << empleado.sexo << endl;
    cout << "Salario: $"<< empleado.salario << endl;
}

struct Empleado * buscarEmpleadoPorNombre(char *nombre){
    char tmp[45];
    strToLower(nombre);
    
    for(int i = 0; i < cantidadEmpleados; i++){
        strcpy(tmp, empleados[i].nombre);
        strToLower(tmp);
        
        if( strcmp(nombre, tmp) == 0 ) {
            return &empleados[i];
        }
    }
    
    return NULL;
}

void agregarEditarEmpleado(struct Empleado *empleado, bool editar){
    if( !editar ){
        empleado->id = generarID();
    }
    
    fflush(stdin);
    
    cout << "Nombre: " << endl;
    cin.getline(empleado->nombre, sizeof(empleado->nombre));
    
    cout << "Sexo: " << endl;
    cin >> empleado->sexo;
    
    cout << "Salario: " << endl;
    cin >> empleado->salario;
    
    showEmpleado(*empleado);
}
